#!/bin/sh             

if [ -z $1 ]
then
	echo "Usage: $0 <channel_port>"
	echo "		channel_port: 1-10"
	echo "output 0x0 to channel"
	exit 1
fi

A=$1
if [ $1 -lt 7 ] ; then
	SYNC=$((97+A))
else
	SYNC=$((99+A))
fi

VALUE=$((A+113))

CLK=110

if [ ! -d /sys/class/gpio/gpio$SYNC ] ; then
	echo $SYNC > /sys/class/gpio/export
	echo "out" > /sys/class/gpio/gpio$SYNC/direction
fi

if [ ! -d /sys/class/gpio/gpio$VALUE ] ; then
	echo $VALUE > /sys/class/gpio/export
	echo "out" > /sys/class/gpio/gpio$VALUE/direction
fi

if [ ! -d /sys/class/gpio/gpio$CLK ] ; then
	echo $CLK > /sys/class/gpio/export
	echo "out" > /sys/class/gpio/gpio$CLK/direction
fi

cd /sys/class/gpio
                      
echo 1 > gpio$SYNC/value
echo 0 > gpio$VALUE/value

echo 1 > gpio$CLK/value
echo 0 > gpio$CLK/value
                      
echo 1 > gpio$CLK/value
echo 0 > gpio$SYNC/value

#1
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value


#2
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

echo 0 > gpio$VALUE/value

#3                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value


#4                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value


#5                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value


#6                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#7                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#8                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#9                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#10                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#11                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#12                      
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value
                      
#13
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#14
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

#15
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value


#16
echo 0 > gpio$CLK/value
echo 1 > gpio$CLK/value

echo 1 > gpio$SYNC/value

