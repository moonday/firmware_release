#!/bin/sh
source /app/app.conf

#echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable
echo $DUTY0 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle

#echo 1 > /sys/class/pwm/pwmchip0/pwm1/enable
echo $DUTY1 > /sys/class/pwm/pwmchip0/pwm1/duty_cycle

#echo 362 > /sys/class/soft_pwm/unexport
#echo $PERIOD2 > /sys/class/soft_pwm/pwm362/period
echo 0 > /sys/class/soft_pwm/pwm362/pulse
#echo 362 > /sys/class/soft_pwm/unexport
