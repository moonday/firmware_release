#!/bin/sh
source /app/app.conf

#echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable
echo $DUTY0 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle

#echo 1 > /sys/class/pwm/pwmchip0/pwm1/enable
echo $DUTY1 > /sys/class/pwm/pwmchip0/pwm1/duty_cycle

#echo 362 > /sys/class/soft_pwm/export
#echo $PERIOD2 > /sys/class/soft_pwm/pwm362/period
echo $DUTY2 > /sys/class/soft_pwm/pwm362/pulse
