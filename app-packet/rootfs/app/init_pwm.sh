#!/bin/sh
source /app/app.conf

echo 0 > /sys/class/pwm/pwmchip0/export
echo 1 > /sys/class/pwm/pwmchip0/export
#sleep 1

echo "channel 0"
echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable
echo $PERIOD0 > /sys/class/pwm/pwmchip0/pwm0/period
#echo 0 > /sys/class/pwm/pwmchip0/pwm0/enable
#sleep 1
echo $DUTY0 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle
#sleep 1
#echo 0 > /sys/class/pwm/pwmchip0/pwm0/enable
#echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable

echo "channel 1"
echo 1 > /sys/class/pwm/pwmchip0/pwm1/enable
#sleep 1
echo $PERIOD1 > /sys/class/pwm/pwmchip0/pwm1/period
#sleep 1
#echo 0 > /sys/class/pwm/pwmchip0/pwm1/enable
echo $DUTY1 > /sys/class/pwm/pwmchip0/pwm1/duty_cycle
#echo 0 > /sys/class/pwm/pwmchip0/pwm1/enable
#echo 1 > /sys/class/pwm/pwmchip0/pwm1/enable

echo 362 > /sys/class/soft_pwm/export
echo $PERIOD2 > /sys/class/soft_pwm/pwm362/period
echo 0 > /sys/class/soft_pwm/pwm362/pulse
#echo 362 > /sys/class/soft_pwm/unexport
