[TOC]

# 内容说明

| 内容 | 对应文件 |
|-----|---------|
| u-boot | u-boot-sunxi-with-spl.bin|
| linux内核 | boot/zImage|
| linux设备树 |boot/*.dtb|
| 根文件系统 | rootfs.tar.gz|


# 操作说明 #


## 系统SDCARD制作 


### 前提条件

#### u-boot编译完成

可下载u-boot-sunxi-with-spl.bin文件。 或自行编译。

源代码位于https://bitbucket.org/moonday/u-boot

配置命令


```
#!shell

make CROSS_COMPILE=arm-linux-gnueabihf- Sinlinx_SinA33_defconfig
```

修改配置命令

```
#!shell

make CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
```


编译命令


```
#!shell

make CROSS_COMPILE=arm-linux-gnueabihf- 
```

#### linux内核编译完成
可从boot目录下下载。

自行编译参考[linux-a33](https://bitbucket.org/moonday/linux-a33)工程下说明
    
#### 根文件系统制作完成
可下载使用roorfs.tar.gz

自行编译参考[buildroot-sinlinx-a33](https://bitbucket.org/moonday/buildroot-sinlinx-a33)工程下说明

### SDCARD分区设置要求 

"start" is a 1k-block number here. (Multiply it by two to get the corresponding sector number - assuming 512 byte sectors).

|start|size|usage|
|-----|----|-----|
|0|8KB|Unused, available for partition table etc.|
|8 	|24KB 	|Initial SPL loader|
|32 	|512KB 	|U-Boot|
|544 	|128KB 	|environment|
|672 	|352KB 	|reserved|
|1024 	|- 	|Free for partitions |


* 获取SDCARD的设备信息

通过下面命令可以获得SDCARD在linux系统中的设备信息和分区信息 export it as ${card}. 


```
#!shell

cat /proc/partitions
```

或


```
#!shell

blkid -c /dev/null
```

If the SD card is connected via USB and is sdX (replace X for a correct letter) 


```
#!shell

export card=/dev/sdX
export p=""

```


If the SD card is connected via mmc and is mmcblk0 


```
#!shell

export card=/dev/mmcblk0
export p=p
```



### 擦除原有数据

擦除SD卡的第一块 (不保留分区表).

```
#!shell

dd if=/dev/zero of=${card} bs=1M count=1
```

如要保留分区表，执行
```
#!shell

dd if=/dev/zero of=${card} bs=1k count=1023 seek=1
```




### 将U-boot写入SDCARD


把u-boot-sunxi-with-spl.bin写入sd-card.

```
#!shell

dd if=u-boot-sunxi-with-spl.bin of=${card} bs=1024 seek=8
```
### 分区SDCARD


```
#!shell

fdisk ${card}
```
用n命令创建分区， 然后根据提示按回车即可
用w命令写分区表并退出

### 格式化SD卡文件系统


```
#!shell

mkfs.ext4 ${card}${p}1
```


### 写根文件系统 


```
#!shell
sudo -i
mount ${card}${p}1 /mnt/
cd /mnt
tar xvf build-root/out/image/rootfs.tar.gz


```


### 拷贝内核

```
#!shell

cp linux/arch/arm/boot/zImage /mnt/boot/
cp linux/arch/arm/boot/dts/sun8i*dtb /mnt/boot

```


### 制作uboot启动脚本
制作/mnt/boot/boot.cmd文件，内容如下

```
#!c

ext4load mmc 0 0x46000000 zImage
ext4load mmc 0 0x49000000 <board>.dtb
setenv bootargs console=ttyS0,115200 earlyprintk root=/dev/mmcblk0p<partition> rootwait panic=10 
bootz 0x46000000 - 0x49000000

```
转换为scr文件 file: boot.scr

```
#!shell



 mkimage -C none -A arm -T script -d boot.cmd boot.scr
 cp boot.scr /mnt/
 umount /mnt/
```



## GPIO操作说明 


* 如要操作引脚PD2，首先，计算PD2在GPIO系统中的编号，计算公式：（引脚端口字母顺序 － 1）X 32 ＋ 引脚号，对于PD2即为98 
* 然后在GPIO文件系统中导出（即允许通过文件操作此引脚），执行如下命令：
```
echo 98 > /sys/class/gpio/export
```
* 这时，系统自动创建对应引脚的目录/sys/class/gpio/gpio98。目录中的direction控制输入输入

```
#!bash

echo "out" > /sys/class/gpio/gpio98/direction
echo 1 > /sys/class/gpio/gpio98/value
```
* 以上命令将PD2设为输出,并输出高电平